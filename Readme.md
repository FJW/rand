Rand - A small tool to create random numbers
============================================

Rand is a small utility to create some high-quality random-numbers.

Usage
-----

	rand min max [n] [unique]

* min and max are the smallest and largest value of the requested double-inclusive range.
* n is the number of requested numbers. If none is given, 1 is used
* unique tells the tool to not repeat already created numbers



