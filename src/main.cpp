#include <iostream>
#include <unordered_set>
#include <functional>
#include <cstdint>
#include <string>
#include <random>
#include <iterator>
#include <algorithm>
#include <vector>
#include <stdexcept>

std::random_device& gen() {
	static std::random_device device;
	return device;
}

std::vector<int> gen_random(int min, int max, std::size_t n) {
	auto dist = std::uniform_int_distribution<int>{min, max};
	auto data = std::vector<int>(n);
	std::generate(data.begin(), data.end(), [&] { return dist(gen()); });
	return data;
}

std::vector<int> gen_random_unique(int min, int max, std::size_t n) {
	const auto range_size = static_cast<std::size_t>(max - min + 1);
	if (range_size < n) {
		throw std::runtime_error{"range to small to provide requested number "
		                         "of unique values"};
	}
	if (range_size < 10 * n) {
		auto vec = std::vector<int>(range_size);
		std::iota(vec.begin(), vec.end(), min);
		std::shuffle(vec.begin(), vec.end(), gen());
		vec.erase(vec.begin() + static_cast<std::ptrdiff_t>(n), vec.end());
		return vec;
	} else {
		auto dist = std::uniform_int_distribution<int>{min, max};
		auto used = std::unordered_set<int>{};
		auto vec = std::vector<int>{};
		while (vec.size() < n) {
			const auto candidat = dist(gen());
			if (used.count(candidat)) {
				continue;
			}
			vec.push_back(candidat);
			used.insert(candidat);
		}
		return vec;
	}
}

void print_numbers(const std::vector<int>& data) {
	if (data.empty()) {
		std::cout << '\n';
		return;
	}
	std::copy(data.begin(), std::prev(data.end()), std::ostream_iterator<int>{std::cout, ", "});
	std::cout << data.back() << '\n';
}

int main(int argc, char** argv) try {
	const auto args = std::vector<std::string>(argv + 1, argv + argc);
	if (args.size() < 2) {
		std::cerr << "Error: invalid number of arguments\n";
		return 1;
	}
	const auto min = std::stoi(args[0]);
	const auto max = std::stoi(args[1]);
	if (max < min) {
		throw std::runtime_error{"min value must not be larger than the max value"};
	}
	const auto n = std::size_t{(args.size() >= 3) ? std::stoul(args[2]) : 1};
	const auto allow_duplicates = (args.size() < 4) ? true : (args[3] != "unique");

	const auto data =
	        allow_duplicates ? gen_random(min, max, n) : gen_random_unique(min, max, n);
	print_numbers(data);
} catch (std::runtime_error& e) {
	std::cerr << "Error: " << e.what() << '\n';
	return 2;
}
